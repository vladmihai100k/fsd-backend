const { Datastore } = require('@google-cloud/datastore');
const datastore = new Datastore({
    projectId: 'fsd-alex-andrei-dani-vlad',
    keyFilename: 'fsd-alex-andrei-dani-vlad.json'
});

// https://us-central1-fsd-alex-andrei-dani-vlad.cloudfunctions.net/helloWorld-1
exports.helloWorld = (req, res) => {
    res.send('Hello World!');
};

// https://us-central1-fsd-alex-andrei-dani-vlad.cloudfunctions.net/addInterval-1
exports.addInterval = (req, res) => {
    res.set('Access-Control-Allow-Origin', '*');
    res.set("Access-Control-Allow-Methods", "POST");
    res.set('Access-Control-Allow-Headers', "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
    res.set('Access-Control-Allow-Credentials', 'true');
    if (req.method === 'OPTIONS') {
        // Send response to OPTIONS requests
        res.set("Access-Control-Allow-Methods", "POST");
        res.set('Access-Control-Allow-Headers', "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
        res.set('Access-Control-Allow-Credentials', 'true');
        res.set('Access-Control-Max-Age', '3600');
        res.status(204).send('');
        return;
    }
    if (!req.body.start_date || !req.body.end_date || !req.body.id) {
        res.status(500).send({ "err": "Missing input", "id": req.body.id });
        return;
    }
    //validate (MM/DD/YYYY), with a year between 1900 and 2099
    var date_regex = "^(?:(0[1-9]|1[012])[\/.](0[1-9]|[12][0-9]|3[01])[\/.](19|20)[0-9]{2})$";
    if (!new RegExp(date_regex).test(req.body.start_date) || !new RegExp(date_regex).test(req.body.end_date)) {
        res.status(500).send({ "err": "Incorrect input", "id": req.body.id });
        return;
    }
    datastore.save({
        key: datastore.key("interval"),
        data: {
            start_date: req.body.start_date,
            end_date: req.body.end_date
        }
    }).catch(err => {
        console.error("Error: ", err);
        res.status(500).send({ "err": err, "id": req.body.id });
        return;
    });
    res.status(200).send({ "message": "Success", "id": req.body.id });

}

// https://us-central1-fsd-alex-andrei-dani-vlad.cloudfunctions.net/getIntervals-1
exports.getIntervals = (req, res) => {
    // try {
    //     const query = datastore.createQuery("interval");
    //     const [intervals] = await datastore.runQuery(query);

    //     res.status(200).send(JSON.stringify(intervals));
    // } catch (err) {
    //     console.error("Error: ", err);
    //     res.status(500).send(err);
    //     return;
    // }
    res.set('Access-Control-Allow-Origin', '*');
    if (req.method === 'OPTIONS') {
        // Send response to OPTIONS requests
        res.set('Access-Control-Allow-Methods', 'GET');
        res.set('Access-Control-Allow-Headers', 'Access-Control-Allow-Origin');
        res.set('Access-Control-Max-Age', '3600');
        res.status(204).send('');
        return;
    }
    const query = datastore.createQuery("interval");
    datastore
        .runQuery(query)
        .then((intervals) => {
            res.status(200).send(JSON.stringify(intervals[0]));
        })
        .catch((err) => {
            console.error("Error: ", err);
            res.status(500).send(err);
        });
};