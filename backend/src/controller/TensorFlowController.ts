import { NextFunction, Request, Response } from "express";
// import  {MnistData} from "../mnist/data.js";
// // const MnistData = require( "../mnist/data.js");
// import {getModel, train} from "../mnist/script.js";
import { MnistData } from "../tensorflow/data";
import { getModel, train, doClassifyPrediction } from "../tensorflow/script";

import * as tf from "@tensorflow/tfjs-node";
import { MODEL_DIR, toArrayBuffer } from "./../tensorflow/utils";

export class TensorFlowController {

    async evaluate(request: Request, response: Response, next: NextFunction) {
        if (request.files == null || request.files.image == null) {
            response.status(400);
            return "No file was sent";
        }

        try {
            response.status(200);
            return await doClassifyPrediction(toArrayBuffer(request.files.image.data));
        } catch (error) {
            response.status(400);
            return error;
        }
    }

    async train(request: Request, response: Response, next: NextFunction) {
        const data = new MnistData();
        await data.load();
        const model = await getModel();
        const result = await train(model, data);
        await model.save(MODEL_DIR);
        response.status(200);
        return result;
        // return response.status(200).send("Train");
    }
}