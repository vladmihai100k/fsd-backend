import { getRepository, UsingJoinTableIsNotAllowedError } from "typeorm";
import { NextFunction, Request, Response } from "express";
import { User } from "../entity/User";
import * as jwt from "jsonwebtoken";

export class UserController {

    private userRepository = getRepository(User);

    async all(request: Request, response: Response, next: NextFunction) {
        return this.userRepository.find();
    }

    async one(request: Request, response: Response, next: NextFunction) {
        return this.userRepository.findOne(request.params.id);
    }

    async save(request: Request, response: Response, next: NextFunction) {
        return this.userRepository.save(request.body);
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        let userToRemove = await this.userRepository.findOne(request.params.id);
        await this.userRepository.remove(userToRemove);
    }

    async login(request: Request, response: Response, next: NextFunction) {
        let user = await this.userRepository.findOne({ where: { email: request.body.email, password: request.body.password } });
        if (!user) {
            return "Incorrect email/password";
        }

        //return "Connected successfully";
        return { token: jwt.sign(user.id, "" + process.env.TOKEN_KEY) };
    }

    async register(request: Request, response: Response, next: NextFunction) {
        let existingEmail = await this.userRepository.findOne({ where: { email: request.body.email } });
        if (existingEmail) {
            return "Email already exists"
        }
        let existingUserName = await this.userRepository.findOne({ where: { userName: request.body.userName } });
        if (existingUserName) {
            return "Username already exists"
        }
        this.userRepository.save(request.body);
        return "Register successfully";
    }

}