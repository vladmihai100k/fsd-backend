import { getRepository } from "typeorm";
import { NextFunction, Request, Response } from "express";
import { User } from "../entity/User";

var jwt = require("jsonwebtoken");

export async function checkToken(request: Request, response: Response, next: NextFunction) {
    try {
        const token = request.headers.authorization.split(" ")[1];
        console.log("Token: " + token);
        const decoded = jwt.verify(token, process.env.TOKEN_KEY);
        request.userData = decoded;
        return next();
    }
    catch (error) {
        console.log("Error: " + error);
        return response.status(401).send("Authentification failed.");
    }
}
