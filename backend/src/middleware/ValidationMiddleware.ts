import { getRepository } from "typeorm";
import { NextFunction, Request, Response } from "express";
import { User } from "../entity/User";
import { isNumber } from "util";

export async function checkLoginInput(request: Request, response: Response, next: NextFunction) {
    if (!request.body.email || !request.body.password) {
        return response.status(403).send("Please fill in all fields");
    }
    return next();
}

export async function checkRegisterInput(request: Request, response: Response, next: NextFunction) {
    if (!request.body.email || !request.body.password || !request.body.userName) {
        return response.status(403).send("Please fill in all fields");
    }
    return next();
}

export async function checkId(request: Request, response: Response, next: NextFunction) {
    if (!request.params.id || isNaN(Number(request.params.id))) {
        return response.status(403).send("Invalid Id.");
    }
    let userRepository = getRepository(User);
    if (!(await userRepository.findOne(request.params.id))) {
        return response.status(403).send("No user with the specified Id.");
    }
    return next();
}