import { TensorFlowController } from "./controller/TensorFlowController";
import { UserController } from "./controller/UserController";
import { checkToken } from "./middleware/AuthMiddleware";
import { checkId, checkLoginInput, checkRegisterInput } from "./middleware/ValidationMiddleware";

export const Routes = [{
    method: "get",
    route: "/users",
    controller: UserController,
    action: "all",
    middlewares: [checkToken]
}, {
    method: "get",
    route: "/users/:id",
    controller: UserController,
    action: "one",
    middlewares: [checkToken, checkId]
}, {
    method: "post",
    route: "/users",
    controller: UserController,
    action: "save",
    middlewares: [checkToken]
}, {
    method: "delete",
    route: "/users/:id",
    controller: UserController,
    action: "remove",
    middlewares: [checkToken, checkId]
}, {
    method: "post",
    route: "/login",
    controller: UserController,
    action: "login",
    middlewares: [checkLoginInput]
}, {
    method: "put",
    route: "/register",
    controller: UserController,
    action: "register",
    middlewares: [checkRegisterInput]
},
{
    method: "post",
    route: "/evaluate",
    controller: TensorFlowController,
    action: "evaluate"
},
{
    method: "post",
    route: "/train",
    controller: TensorFlowController,
    action: "train"
}
];