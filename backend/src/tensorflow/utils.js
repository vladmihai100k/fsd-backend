export const MODEL_DIR = 'file://./model';
export const MODEL_PATH = 'file://./model/model.json';

export function toArrayBuffer(buffer) {
    var ab = new ArrayBuffer(buffer.length);
    var view = new Uint8Array(ab);

    for (let i = 0; i < buffer.length; i++) {
        view[i] = buffer[i];
    }

    return ab;
}