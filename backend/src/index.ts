import "reflect-metadata";
import { createConnection } from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import { Request, Response } from "express";
import { Routes } from "./routes";
import { User } from "./entity/User";

createConnection().then(async connection => {

    // create express app
    const app = express();
    app.use(bodyParser.json());
    var cors = require("cors");
    app.use(cors());
    var fileUpload = require("express-fileupload");
    app.use(fileUpload());

    var router = express.Router();

    // register express routes from defined application routes
    Routes.forEach(route => {
        if (route.middlewares) {
            route.middlewares.forEach((middleware) => {
                (router as any)[route.method](route.route, middleware);
            });
        }

        (router as any)[route.method](route.route, (req: Request, res: Response, next: Function) => {
            const result = (new (route.controller as any))[route.action](req, res, next);
            if (result instanceof Promise) {
                result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);

            } else if (result !== null && result !== undefined) {
                res.json(result);
            }
        });
    });

    // mount the router on the app
    app.use('/', router);
    require("dotenv").config();

    // setup express app here
    // ...

    // start express server
    app.listen(3000);

    // insert new users for test
    // await connection.manager.save(connection.manager.create(User, {
    //     email: "ab@yahoo.com",
    //     userName: "Andrei",
    //     password: "1234"
    // }));
    // await connection.manager.save(connection.manager.create(User, {
    //     email: "ds@gmail.com",
    //     userName: "Danii",
    //     password: "123123"
    // }));

    console.log("Express server has started on port 3000. Open http://localhost:3000/users to see results");

}).catch(error => console.log(error));
